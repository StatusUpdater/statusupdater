/*******************************************
* Cory Gordinier                           *
* EECS 285                                 *
* 11/26/11                                 *
* Project 4                                *
*******************************************/

package eecs285.proj4.cgordini;

import java.io.*;

import javax.swing.*;

public class UMichStatusUpdater
{    
     public static FacebookClient fbClient;
     public static TwitterClient twitterClient;
     public static TwitBook_GUI mainGui;

     //Still have to establish all the GUI stuff, link that in!
     public static void main(String args[]) 
     {
          fbClient = new FacebookClient(false);
          twitterClient = new TwitterClient(false);
          
          mainGui = new TwitBook_GUI(fbClient, twitterClient, false);

          fbClient.setMainFrame(mainGui);
          twitterClient.setMainFrame(mainGui);

          //READ FROM FILE, HANDLE PRE-EXISTING TOKENS
          load(fbClient, twitterClient);
                  
          

     }//end main
     
     //Kinda crappy load code -- refactor into something more efficient?
     //Per class basis?
     public static void load(FacebookClient fbClient, 
    		                 TwitterClient twitterClient)
     {
         //load data from file (if existing
    	 File inFile = new File("Facebook.txt");
         if(inFile.exists())
         {
             //loading = true;
             FileReader fileReader = null;
		     try 
		     {
		         fileReader = new FileReader(inFile);
		     } 
		     catch (FileNotFoundException e) 
		     {
			      // TODO Auto-generated catch block
			      e.printStackTrace();
		     }
             BufferedReader bFileReader = new BufferedReader(fileReader);
             try 
             {
		         System.out.println("FB Client attempt");
		         fbClient.setSession(bFileReader.readLine());
		         fbClient.setSecret(bFileReader.readLine());
		         fbClient.authenticate();
		         System.out.println("FB Client authenticated!");
                 mainGui.fbEnabled();
		     }  
             catch (IOException e) 
             {
		    	// TODO Auto-generated catch block
		    	e.printStackTrace();
		     }
         }//end if
         
         //load data from file (if existing
    	 inFile = new File("Twitter.txt");
         if(inFile.exists())
         {
             //loading = true;
             FileReader fileReader = null;
		     try 
		     {
		         fileReader = new FileReader(inFile);
		     } 
		     catch (FileNotFoundException e) 
		     {
			      // TODO Auto-generated catch block
			      e.printStackTrace();
		     }
             BufferedReader bFileReader = new BufferedReader(fileReader);
             try 
             {
		         System.out.println("Twitter Client attempt");
		         twitterClient.setToken(bFileReader.readLine());
		         twitterClient.setSecret(bFileReader.readLine());
		         twitterClient.authenticate();
		         System.out.println("Twitter Client authenticated!");
                 mainGui.twEnabled();
		     }  
             catch (IOException e) 
             {
		    	// TODO Auto-generated catch block
		    	e.printStackTrace();
		     }
         }//end if
         
     }//end load
     
}//end class
