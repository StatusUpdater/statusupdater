/*******************************************
* Cory Gordinier                           *
* EECS 285                                 *
* 11/26/11                                 *
* Project 4                                *
*******************************************/

package eecs285.proj4.cgordini;

import java.io.*;

import javax.swing.*;

public class UMichStatusUpdaterApplet extends JApplet
{    
     public FacebookClient fbClient;
     public TwitterClient twitterClient;
     public TwitBook_GUI mainGui;

     //Still have to establish all the GUI stuff, link that in!
     public void init() 
     {
          fbClient = new FacebookClient(true);
          twitterClient = new TwitterClient(true);
          
          mainGui = new TwitBook_GUI(fbClient, twitterClient, true);

          fbClient.setMainFrame(mainGui);
          twitterClient.setMainFrame(mainGui);
          fbClient.setApplet(this);
          twitterClient.setApplet(this);
            
     }//end main
     
}//end class
