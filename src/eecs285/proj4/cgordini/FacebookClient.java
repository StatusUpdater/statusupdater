/*******************************************
* Cory Gordinier                           *
* EECS 285                                 *
* 11/26/11                                 *
* Project 4                                *
*******************************************/

package eecs285.proj4.cgordini;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.*;

import com.google.code.facebookapi.*;
//import java.io.IOException;

public class FacebookClient extends SocialClient
{    
     public static String API_KEY = "276913232351592"; //get from FB
     //get from FB -- see all the problems with this!
     public static String SECRET = "01ce33efadea43e0a844d139d145d6d5";
     protected static FacebookJsonRestClient client;
     private String session = "";
     
     public FacebookClient(boolean inIsApplet)
     {
    	  client = new FacebookJsonRestClient(API_KEY, SECRET);
          isEnabled = false;
          isLoggedIn = false;
          isApplet = inIsApplet;
     }//end constructor
     

     public void logout()
     {
       if(!isApplet)
       {
         File file = new File("Facebook.txt");
         if(file.exists())
         {
           try
	       {
             file.delete();
	       } 
		   catch (Exception e) 
	       {
             System.out.println("Facebook File read-only");
	         e.printStackTrace();
           }
         }
       }
       mainFrame.fbLoggedOut();
     }

     public void setEnabled()
     {
       super.setEnabled();
       mainFrame.fbEnabled();
     }

     public void setDisabled()
     {
       super.setDisabled();
       mainFrame.fbDisabled();
     }


     public void login()
     {
        if(!isLoggedIn)
        {
          // Create the initial client instance;
          // uses a non-secure SECRET key!
          //client = new FacebookJsonRestClient(API_KEY, SECRET);

          try 
          {
               //create a token for use by the remade client
               token = client.auth_createToken();
                 
               //Authorize a FB session!
               String sessionUrl = 
                    "http://www.facebook.com/login.php?api_key="
                    + API_KEY + "&v=1.0?"
                    + "&auth_token=" + token;
                 
               // Open an external browser to login
               openSessionUrl(sessionUrl);
               
               if (sessionUrl != null)
               {
                   String permissionUrl = 
                   "http://www.facebook.com/authorize.php?api_key="
            			                  + API_KEY
            			                  + "&v=1.0&ext_perm="
            			                  + "offline_access,status_update";

                    openSessionUrl(permissionUrl);
               }

               boolean logged_in = false;
               int i = 0; //remove
               while(!logged_in) //continue looking into this -- Cory
               {
              	 System.out.println("Attempt #" + i);
                   try
                   {
                       session = client.auth_getSession(token, true);
                       System.out.println(session);
                       logged_in = true;
                   }
                   catch(Exception e) //FacebookException?
                   {
                   }
                   i++;
               }//end loop    
               
               // obtain temp secret (need it for desktop apps!)
               tokenSecret = client.getCacheSessionSecret(); 
               
               if(!isApplet)
               {
                 makeCookie(session, tokenSecret);
               }

               authenticate();
               
           } 
           catch (FacebookException e) 
           {
               // TODO Auto-generated catch block
               e.printStackTrace();
           } 
       }//if !isLoggedIn

     } // end login

     public void authenticate()
     {
         // fetch session key
    	 if (tokenSecret != null && tokenSecret != "" && tokenSecret != "NONE"
    	     && session != null && session != "" && session != "NONE")
    	 {
             //reset client with tempSecret and session key
             client = new FacebookJsonRestClient(API_KEY,
            		                             tokenSecret, session);
       
             //once all this is done, 
             //the CLIENT object is usable!
             //throw exception if you couldn't log in/the token was
             //invalid or empty?
             isLoggedIn = true;
             isEnabled = true;
    	 }//end if
     }
     
     public void makeCookie(String inSession, String inTempSecret)
     {
         try
         {
        	  //can do some CRAZY encryption before printing to file...
        	  PrintStream outFile = new PrintStream("Facebook.txt");
              outFile.println(inSession);
              outFile.println(inTempSecret);
              outFile.close();
         }
         catch (FileNotFoundException except)
         {
              System.out.println("Unable to open output file");
         }
     }
     
     public void setSession(String inSession)
     {
    	 this.session = inSession;
     }

     public boolean setStatus(String status)
     {
    	 try
    	 {
              if (client.users_hasAppPermission(Permission.STATUS_UPDATE)) 
              {
        	       //Post status; doesn't clear but DOES remove "is"
                   return client.users_setStatus(status, false, true);
              }
    	 }
    	 catch (FacebookException e)
         {
             e.printStackTrace();
         }
    	 return false;
    	 
     }//end method
     
}//end class
