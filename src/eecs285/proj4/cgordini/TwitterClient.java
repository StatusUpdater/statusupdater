/*******************************************
* Cory Gordinier                           *
* EECS 285                                 *
* 11/26/11                                 *
* Project 4                                *
*******************************************/

package eecs285.proj4.cgordini;

import java.io.*;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.*;

public class TwitterClient extends SocialClient
{    
	 public static String CONSUMER_KEY = 
			          "u6fPJMkJDrxgZGmglu75gg"; //get from Twitter
     public static String CONSUMER_SECRET = 
    		          "e8918ZfSopNEkclo0OtKMKdh7qWBBZ5qWFtEcFAk0E";
     protected static Twitter twitter;

     // for pop-up alignment
     public PromptDialog pinPrompt;
     
     public TwitterClient(boolean inIsApplet)
     {
    	  //You'll ALWAYS need it. Probably. Unless you don't want to Twitter.
          twitter = new TwitterFactory().getInstance();
          twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET); //needs t/c?
          isEnabled = false;
          isLoggedIn = false;
          isApplet = inIsApplet;
     }//end ctor


     public void setEnabled()
     {
       super.setEnabled();
       mainFrame.twEnabled();
     }

     public void setDisabled()
     {
       super.setDisabled();
       mainFrame.twDisabled();
     }


     public void logout()
     {
       if(!isApplet)
       {
         File file = new File("Twitter.txt");
         if(file.exists())
         {
           try
	       {
             file.delete();
	       } 
		   catch (Exception e) 
	       {
             System.out.println("Twitter File read-only");
	         e.printStackTrace();
           }
         }
       }
       mainFrame.twLoggedOut();
     }


     public void login()
     {
       if(!isLoggedIn)
       {
    	 try 
    	 {
              //twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
              try 
              {
                 // get request token.
                 // this will throw IllegalStateException w/ existing accessT.
            	  
                 RequestToken requestToken = twitter.getOAuthRequestToken();
                 System.out.println("Request token: " + 
                                    requestToken.getToken());
                 System.out.println("Request token secret: " + 
                                    requestToken.getTokenSecret());
                 AccessToken accessToken = null;

                 BufferedReader br = new BufferedReader(
                		                     new InputStreamReader(System.in));
                 while (accessToken == null) 
                 {
                      openSessionUrl(requestToken.getAuthorizationURL());
                      //REQUIRES PIN?!

                      // pop-up for pin
                      pinPrompt = new PromptDialog(mainFrame, "Twitter PIN",
                              "Copy and Paste PIN here after logging in");
                      String pin = pinPrompt.getText();


                      /* //Replacing with pop-up ^
                      System.out.print("Enter the PIN(if available) and hit " +
                      		          "enter after you granted access.[PIN]:");
                      String pin = br.readLine(); //replace with popup!
                      */


                      try 
                      {
                         if (pin.length() > 0) 
                         {
                             accessToken = 
                                twitter.getOAuthAccessToken(requestToken, pin);
                         } 
                         else //will always go this route?
                         {
                             accessToken = 
                            	twitter.getOAuthAccessToken(requestToken);
                         }
                      } 
                      catch (TwitterException e) 
                      {
                         if (401 == e.getStatusCode()) 
                         {
                             System.out.println("Unable to get the access token.");
                         } 
                         else 
                         {
                             e.printStackTrace();
                         }
                     }//end catch
                 }
                 System.out.println("Access token: " + 
                                    accessToken.getToken());
                 System.out.println("Access token secret: " + 
                                    accessToken.getTokenSecret());
                 this.token = accessToken.getToken();
                 
                 this.setSecret(accessToken.getTokenSecret());

                 if(!isApplet)
                 {
                   makeCookie(this.token, this.tokenSecret);
                 }

                 authenticate();//arguably redundant, but makes sure 
                                //login is finished AND AT/ATS work.
             } 
             catch (IllegalStateException ie) 
             {
                 // access token is already available, or consumer key/secret is not set.
                 if (!twitter.getAuthorization().isEnabled()) 
                 {
                     System.out.println("OAuth consumer key/secret is not set.");
                     System.exit(-1);
                 }
             }//end catch
//             catch (IOException e) 
//             {
//			     // TODO Auto-generated catch block
//			     e.printStackTrace();
//		     }
         }//end try
    	 catch (TwitterException e) 
    	 {
              e.printStackTrace();
         }//end catch

       }

     } // end login

     public void authenticate()
     {
        AccessToken accessToken = null;
        if (token != "NONE" && token != "" && token != null 
          && tokenSecret != "NONE" && tokenSecret != "" && tokenSecret != null)
        {
		    accessToken = new AccessToken(token, tokenSecret);
    	    twitter.setOAuthAccessToken(accessToken); 
    	
            //once all this is done, 
            //the CLIENT object is usable!
            //throw exception if you couldn't log in/the token was
            //invalid or empty?
            this.setLoggedIn();
            this.setEnabled();
        }
     }//end useToken
     
     public void setSecret(String inTokenSecret)
     {
    	 this.tokenSecret = inTokenSecret;
     }
     
     public void makeCookie(String inToken, String inTokenSecret)
     {
         try
         {
        	  //can do some CRAZY encryption before printing to file...
        	  PrintStream outFile = new PrintStream("Twitter.txt");
              outFile.println(inToken);
              outFile.println(inTokenSecret);
              outFile.close();
         }
         catch (FileNotFoundException except)
         {
              System.out.println("Unable to open output file");
         }
     }

     public boolean setStatus(String status)
     {
          Status statusObj = null;
		  try 
		  {
		       statusObj = twitter.updateStatus(status);
		  } 
		  catch (TwitterException e) 
		  {
			// TODO Auto-generated catch block
		       e.printStackTrace(); //remove?
		       return false;
		  }
		  if (statusObj != null) //remove block
		  {
               System.out.println("Successfully updated the status to [" + statusObj.getText() + "].");
               return true;
		  }
          return false;
     }
     
}//end class
