/**
 *  SeedDialog.java - a dialog to get a random seed
 *
 *   Author: Shane DeMeulenaere
 *  Revised: November 2011
 *
 *
 */

package eecs285.proj4.cgordini;

import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import javax.swing.*;


public class PromptDialog extends JDialog
{

  JPanel mainPanel;
  JPanel buttonPanel;
  JLabel promptLabel;
  JTextField textField;
  JButton okButton;
  
  public PromptDialog(JFrame mainFrame, String title, String prompt)
  {
    super(mainFrame, title, true);  // create JDialog that is modal
   
    mainPanel = new JPanel();
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    
    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());


    promptLabel = new JLabel(prompt);
    //promptLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    mainPanel.add(new JLabel(prompt));

    textField = new JTextField(20);
    //textField.setAlignmentX(Component.LEFT_ALIGNMENT);
    mainPanel.add(textField);
    
    okButton = new JButton(" OK ");
    okButton.addActionListener(new ActionListener()
                                   {
                                     public void actionPerformed(ActionEvent e)
                                     {
                                       if(!textField.getText().isEmpty())
                                       {
                                         setVisible(false);
                                       }
                                     }
                                   });
    //buttonPanel.add(okButton);
    //mainPanel.add(buttonPanel);
    //okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    mainPanel.add(okButton);


    add(mainPanel);
    pack();
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    mainPanel.setVisible(true);
    setVisible(true);
  }


  public String getText()
  {
    return textField.getText();
  }

  

}
