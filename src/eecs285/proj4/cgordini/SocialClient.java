/*******************************************
* Cory Gordinier                           *
* EECS 285                                 *
* 11/26/11                                 *
* Project 4                                *
*******************************************/

package eecs285.proj4.cgordini;

import java.net.*;
import javax.swing.*;

public abstract class SocialClient
{    
     //allow for enabling/disabling a client
     protected boolean isEnabled = true; 

     protected boolean isLoggedIn;

     protected String token = ""; //string to store user token
     protected String tokenSecret = "";
     public TwitBook_GUI mainFrame;
     public boolean isApplet;
     public JApplet applet;
     
     //public boolean setStatus(String status)
     //{
     //     return true; //won't ever fail...
     //}

     public abstract void logout();

     public void setApplet(JApplet inApplet)
     {
       applet = inApplet;
     }

     public void setMainFrame(TwitBook_GUI inMainFrame)
     {
       mainFrame = inMainFrame;
     }

     
     protected void openSessionUrl(String url)
     {
//    	 try
//    	 {
            if(!isApplet)
            {
              BareBonesBrowserLaunch.openURL(url);
            }
            else
            {
              // javascript call
              try
              {
                applet.getAppletContext().showDocument
        (new URL("javascript:openURL(\"" + url +"\")"));
      }
    catch (MalformedURLException me) { }
            }

              // Wait until the login process is completed?
              //System.out.println("Use browser to login then press return");
              //System.in.read();    	 }
              
//              Thread.currentThread();
//              Thread.sleep(5000);//sleep for 5 seconds.
//         }
//         catch(InterruptedException e)
//         {
//             e.printStackTrace();
//         }
    	 
     }//end method


     public abstract void login();
     

     public boolean getLoggedInStatus()
     {
       return this.isLoggedIn;
     }


     public void setEnabled()
     {
          this.isEnabled = true; 
     }
     
     public void setDisabled()
     {
          this.isEnabled = false; 
     }
     
     public boolean getEnabledStatus()
     {
    	return this.isEnabled; 
     }
     
     public void setLoggedIn()
     {
          this.isLoggedIn = true; 
     }
     
     public void setLoggedOut()
     {
          this.isLoggedIn = false; 
     }
     
     public boolean getLoginStatus()
     {
    	return this.isLoggedIn; 
     }
     
     public void setToken(String inToken)
     {
    	this.token = inToken;
     }
     
     public void setSecret(String inTokenSecret)
     {
    	 this.tokenSecret = inTokenSecret;
     }
     
}//end class
