
package eecs285.proj4.cgordini;

import javax.swing.*;
import java.awt.event.*;


public class ClientListener extends MouseAdapter
{
  public JPopupMenu popup;
  public SocialClient client;

  public ClientListener(SocialClient inClient)
  {
    client = inClient;
  }

  public void mousePressed(MouseEvent e)
  {
    maybeShowPopup(e);
  }

  public void mouseReleased(MouseEvent e)
  {
    maybeShowPopup(e);
  }


  public void mouseClicked(MouseEvent e)
  {
    if(!e.isPopupTrigger())
    {
      if(!client.getLoginStatus())
      {
System.out.println("login");
        client.login();
        client.setEnabled();
      }
      else if(client.getEnabledStatus())
      {
System.out.println("disable");
        client.setDisabled();
      }
      else
      {
System.out.println("enable");
        client.setEnabled();
      }
    }
  }


  private void maybeShowPopup(MouseEvent e)
  {
    if(e.isPopupTrigger() & client.getLoggedInStatus())
    {
      popup = new JPopupMenu();
      JMenuItem menu = new JMenuItem("Logout");
      menu.addActionListener(new ActionListener()
                            {
                              public void actionPerformed(ActionEvent e)
                              {
System.out.println("logout");
                                TwitBook_GUI mainFrame = client.mainFrame;
                                boolean isApplet = client.isApplet;
                                JApplet applet = client.applet;
                                client.logout();
                                if(client instanceof FacebookClient)
                                {
                                  client = new FacebookClient(isApplet);
                                  client.setMainFrame(mainFrame);
                                  client.applet = applet;
                                }
                                else // twitter
                                {
                                  client = new TwitterClient(isApplet);
                                  client.setMainFrame(mainFrame);
                                  client.applet = applet;
                                }
                              }
                            });
      popup.add(menu);
      popup.show(e.getComponent(), e.getX(), e.getY());
    }

  }


}
